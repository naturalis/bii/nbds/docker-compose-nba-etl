#!/bin/bash
#
# run_infuser.sh
#
echo "$(date -Iseconds)|INFO |run_etl.sh: started"
for pid in $(pidof -x run_etl.sh); do
  if [ $pid != $$ ]; then
    echo "$(date -Iseconds)|INFO |run_etl.sh: Process is already running with PID $pid"
    exit 1
  fi
done

cd /opt/compose_projects/etl/compose/
sudo docker compose run etl ./run-job
echo "$(date -Iseconds)|INFO |run_etl.sh: finished"

