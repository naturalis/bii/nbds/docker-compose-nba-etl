#!/bin/bash

if [[ $(whoami) != "root" ]]; then
  echo "ERROR: Please run script as root user!"
  exit 1
fi

start=$(date +%s)
cd /opt/compose_projects/etl/compose
docker compose down
docker system prune -a -f
docker compose pull
docker compose up -d
end=$(date +%s)
echo "Docker system prune finished in: $(($end-$start)) seconds"
